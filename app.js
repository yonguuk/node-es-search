const express = require('express');

const app = express();
app.set('port', process.env.PORT || 8181);

app.get('/', (req, res, next) => {
    res.status(200).send('Welcome!');
});

app.use((req, res, next) => {
    const error = new Error(`${req.method} ${req.url}과 일치하는 라우터가 없습니다.`);
    error.status = 400;
    next(error);
});

app.use((err, req, res, next) => {
    console.log(err);
});

app.listen(app.get('port'));